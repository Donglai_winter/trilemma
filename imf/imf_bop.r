# The file is to download the changes in the financial account
# and the financial account

# Working codes starts from line #165, staring from setwd

# load required packages
# clear all variables
rm(list = ls(all = TRUE))
# Install other related package

libraries = c("IMFData", "countrycode", "dplyr", "zoo", "jsonlite", "RJSONIO","rjson", "httr","devtools")

lapply(libraries, function(x) if (!(x %in% installed.packages())) {
    install.packages(x)
})
lapply(libraries, library, quietly = TRUE, character.only = TRUE)


###################################################################

# rewrite compactdatamethod
CompactDataMethod <- function(databaseID, queryfilter = NULL,
                              startdate = '2001-01-01', enddate = '2001-12-31',
                              checkquery = FALSE, verbose = FALSE, tidy = FALSE, acceptedquery = dsm) {
    if (checkquery) {
        if (!is.element(paste("DS-", databaseID, sep = ""), dataflow_parsed$id)) {
            stop('databaseID missing in API')
            return(NULL)
        }
        print('databaseID is in API')
    }
    
    # Bug fixed. Here used to be a logic error.
    if (checkquery) {
        if (length(queryfilter) != 0 &&
            length(queryfilter) != length(acceptedquery)) {
            stop('queryfilter is wrong format')
            return(NULL)
        } else { print('queryfilter format is OK') }
    }
        
    queryfilterstr <- ''
    if (length(queryfilter) > 0) {
        queryfilterstr <- paste0(
      unlist(plyr::llply(queryfilter,
                         function(x)(paste0(x, collapse = "+")))), collapse = ".")
                         }

    APIstr <- paste0('http://dataservices.imf.org/REST/SDMX_JSON.svc/CompactData/',
                    databaseID, '/', queryfilterstr,
                    '?startPeriod=', startdate, '&endPeriod=', enddate)
    r <- httr::GET(APIstr, httr::add_headers('user-agent' = ''))

    if (verbose) {
        cat('\nmaking API call:\n')
        cat(APIstr)
        cat('\n')
    }

    if (httr::http_status(r)$reason != "OK") {
        stop(paste(unlist(httr::http_status(r))))
        return(list())
    }
    r.parsed <- jsonlite::fromJSON(httr::content(r, "text"))

    if (is.null(r.parsed$CompactData$DataSet$Series)) {
        warning("No data available")
        return(NULL)
    }

    if (class(r.parsed$CompactData$DataSet$Series) == "data.frame") {
        r.parsed$CompactData$DataSet$Series <- r.parsed$CompactData$DataSet$Series[!plyr::laply(r.parsed$CompactData$DataSet$Series$Obs, is.null),]
        if (nrow(r.parsed$CompactData$DataSet$Series) == 0) {
            warning("No data available")
            return(NULL)
        }
    }

    if (class(r.parsed$CompactData$DataSet$Series) == "list") {
        if (is.null(r.parsed$CompactData$DataSet$Series$Obs)) {
            warning("No data available")
            return(NULL)
        }
        ret.df <- as.data.frame(r.parsed$CompactData$DataSet$Series[1:(length(r.parsed$CompactData$DataSet$Series) - 1)])
        ret.df$Obs <- list(r.parsed$CompactData$DataSet$Series$Obs)
        names(ret.df) <- names(r.parsed$CompactData$DataSet$Series)
        r.parsed$CompactData$DataSet$Series <- ret.df
    }

    if (tidy) {
        ret.df <- r.parsed$CompactData$DataSet$Series
        for (i in 1:length(ret.df$Obs)) {
            ret.df$Obs[[i]] <- merge(ret.df$Obs[[i]], ret.df[i, 1:(ncol(ret.df) - 1)])
        }
        ret.df <- plyr::ldply(ret.df$Obs)
        return(ret.df)
    }

    return(r.parsed$CompactData$DataSet$Series)
}

# make a function to accomodate the exhaustive search:
# Here the exhaustive refers only to the country list
exhaustive_bop <- function(db_id = "BOP", indicator = "IOW_DV_BP6_USD", startdate = "1900-01-01", enddate = "2017-12-31", CL_FREQ = "A") {
    # generate a sample for both container and testing 
    queryfilter_sample <- list(CL_FREQ = CL_FREQ, CL_AREA_BOP = "US", CL_INDICATOR_BOP = indicator)
    sample_query <- CompactDataMethod(db_id, queryfilter_sample, startdate, enddate, checkquery = TRUE, tidy = TRUE)
    if (!is.null(sample_query)) {
        print(c("sample generated successfully"))
    } else { return(NULL) }

    # get useful index for the countries/regions
    # usually all the available countries are in the column 2 of the available codes
    report_list <- data.frame(available.codes[2])
    #ref_list <- data.frame(CDIS.available.codes[4])
    l_report <- dim(report_list)[1]
    #l_ref <- dim(ref_list)[1]


    # loop for all available country pairs
    # get report country list

    # use US-CN as the container template
    temp_df <- sample_query[FALSE,]
    name_df <- names(temp_df)


    for (i in 1:l_report) {
        queryfilter <- list(CL_FREQ = "A", CL_AREA_BOP_2017M05 = report_list[i, 1], CL_INDICATOR_CDIS = indicator)
        try(query <- CompactDataMethod(db_id, queryfilter, startdate, enddate, checkquery = FALSE, tidy = TRUE))
        if (!is.null(query)) {
            #names(query) <- name_df  the number of column changes in some countries such CZ 
            temp_df <- merge(temp_df, query, all = TRUE)
            print(c(i, "successful", report_list[i, 1], indicator))
        } else { print(c(i, "failed", report_list[i, 1], indicator)) }

    }

    csvname <- paste0(indicator, ".csv")
    write.csv(temp_df, file = csvname)
    return(temp_df)
}

########################################################################
# rewrite get-dataflow-method
URL <- "http://dataservices.imf.org/REST/SDMX_JSON.svc/Dataflow"
raw_download <- RETRY('GET', URL, user_agent(''), progress(), times = 3)
content_raw <- content(raw_download, "parsed")
dataflow_unparsed <- content_raw$Structure$Dataflows$Dataflow
l_dflow <- length(dataflow_unparsed)

# get available database
dataflow_parsed <- data.frame(id = character(), version = character(), agencyid = character(), lang = character(), text = character(), key_ref = character(), key_ref_agent = character())
name_parsed <- names(dataflow_parsed)

for (i in 1:l_dflow) {
    df <- data.frame(dataflow_unparsed[[i]][1], dataflow_unparsed[[i]][2], dataflow_unparsed[[i]][3], dataflow_unparsed[[i]][6], dataflow_unparsed[[i]][7])
    names(df) <- name_parsed
    dataflow_parsed <- rbind(dataflow_parsed, df)
}
print(dataflow_parsed)
dataflow_parsed$text
dataflow_parsed$id

########################################################################
########################################################################
########################################################################


# set working directory
setwd("D:/dropbox/Dropbox/LKY_RA/trilemma/imf/")

BOP.available.codes <- DataStructureMethod("BOP")
# BOP.available.codes <- DataStructureMethod("BOPAGG_2017")
# check available dimensions
names(BOP.available.codes)
# to check relevant variables
BOP.available.codes[3]
# As there are around 5000 variables, write it to CSV file.
write.csv(BOP.available.codes[3], file = "BOP_code.csv")
# Failed to write directly to xlsx file due to system incompatiability

# extract data
# for FDI inflow and outflow. Choose: 33  Inward Direct Investment Positions, Derived, US Dollars

# taking US-CN for example
data_list_bop <- list(CL_FREQ = "A", CL_AREA_BOP = "US", CL_INDICATOR_BOP = c("BFDA_BP6_USD","BFDL_BP6_USD"))

# important paras. for length of query format
db_id1 <- "BOP"  # source database anchored to "BOP". 
dsm <- DataStructureMethod("BOP") 
startdate <- "1900-01-01"
enddate <- Sys.Date()

bop_query <- CompactDataMethod(db_id1, data_list_bop, startdate, enddate, checkquery = TRUE, tidy = TRUE)

# important object for exhaustive functions
available.codes <- DataStructureMethod(db_id1)

list_indicators <- c("BFDA_BP6_USD",
                     "BFDL_BP6_USD",
                    "BFPAD_BP6_USD",
                     "BFPAE_BP6_USD",
                     "BFPA_BP6_USD",
                     "BFPLD_BP6_USD",
                     "BFPLE_BP6_USD",
                     "BFPL_BP6_USD",
                     "BFRA_BP6_USD")



# Exhaustive search to download data
# Since we need to try different varibles, a loop for each variable is designed instead of combining all variables into one query
# Please put all variables into one list, like the list_indicators.


for (indi in list_indicators) {
    try(all_pair <- exhaustive_bop(db_id = db_id1, indicator = indi))
    }
# print list of variable list and text name
# usually the 3rd column is the info for indicators
combined_id <- CodeSearch(available.codes, names(available.codes)[3], "BFDA_BP6_USD")
# just to generate a sample of the indicator as a method of testing
combined_id <- combine_id[FALSE,]
for (indi in list_indicators) {
    try(dataflow_id <- CodeSearch(available.codes, names(available.codes)[3], indi))
    combined_id <- merge(dataflow_id, combined_id, all=TRUE)
}
print(combined_id)
write.csv(combined_id,file="id.csv")



# added section for combining data --------------------------------------
# read data according to id.csv
setwd("D:/dropbox/Dropbox/LKY_RA/trilemma/imf/")
id <- read.csv("id.csv",stringsAsFactors = FALSE)

for (i in id$CodeValue) {
    csv_data <- read.csv(paste(i,".csv",sep=""))
    assign(i, csv_data)
}

# inspect the structure of each dataset, found that the structure is similar
# rename the value for each dataset for further merger
# Make a container of the df
list_df = list(data.frame())
k = 0
for (i in id$CodeValue) {
    k = k + 1
    df0 <- get(i)
    df <- df0[,c("X.OBS_VALUE", "X.TIME_PERIOD", "X.REF_AREA")]
    names(df)[names(df) == "X.OBS_VALUE"] <- paste0(i, "_value")
    assign(i, df)
    #generate a column for id = year+country
    df[,'id'] <- paste0(df$X.TIME_PERIOD, df$X.REF_AREA)
    # fill in the container for later use
    list_df[[k]] <- df
}

# merge all the dataset by year and reporting country
merged_bop <- Reduce(function(x, y) merge(x, y, by = c("id", "X.TIME_PERIOD", "X.REF_AREA"), all = TRUE), list_df)

# load iso file and match for iso2 to iso3
iso <- read.csv("iso.csv", header = TRUE)

# left join to preserve all records in merged bop
merged_bop_iso3 <- merge(x = merged_bop, by.x = 'X.REF_AREA', y =  iso, by.y = 'iso2', all.x = TRUE)
write.csv(merged_bop_iso3, "merged_bop_iso3.csv")