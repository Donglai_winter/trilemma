# clear all variables
rm(list = ls(all = TRUE))
# Install IMF data and other related package
libraries = c("imfr", "plyr", "countrycode", "IMFData")
lapply(libraries, function(x) if (!(x %in% installed.packages())) {
  install.packages(x)
})
lapply(libraries, library, quietly = TRUE, character.only = TRUE)
#setwd
setwd("D:/Donglai/bitbucket/Trilemma/")

# matching country and variable list from fx_ir_conn data
fx_ir <- read.csv("d:/dropbox/Dropbox/InProgress/Data/Trilemma/fx_ir_source_data.csv")
#store country name into list_iso3
list_iso3<-names(summary(fx_ir$Iso.Country))

# For each country find the right ISO-2 code which IMF uses.
list_iso2 <- countrycode(list_iso3,"iso3c","iso2c")

# get the data structure for BOP
dstr_bop =  DataStructureMethod("BOP")
names(dstr_bop)                    
CodeSearch(dstr_bop, "CL_INDICATOR_BOP" , "BFPAE_BP6_USD")

