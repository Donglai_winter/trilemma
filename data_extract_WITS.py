# import required pkg #
import pandas as pd
import pandasdmx
from pandasdmx import Request as rqst 

# show available database from pandaSDMX
rqst.list_agencies()

# Dive into wits data
wits = 'WBG_WITS'
wits_rqst = rqst('WBG_WITS')
wits_categoryscheme = wits_rqst.categoryscheme()  

# Show dataflow from wits
wits_dataflow_list = wits_categoryscheme.write().dataflow
# A glance at the dataflow def_list
wits_categoryscheme.write().dataflow
# a glance at the category scheme
wits_categoryscheme.write().categoryscheme

# Show particular dataflow def. (DFD) for dataflow of interest
# we are interested in the capital in/out-flow so should be in the financial sector
list(wits_categoryscheme.categoryscheme.WITS_Data['WITS_TradeStats']) 
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
 print(wits_dataflow_list) #read dataflow_id from here

# Getting the data structure definition (DSD)
dsd_DF_WITS_TradeStats_Trade_id = wits_categoryscheme.dataflow.DF_WITS_TradeStats_Trade.structure.id # it is actually 'TRADESTATS'
#refs = dict(reference = 'all')
dsd_wits_response = wits_rqst.datastructure(dsd_DF_WITS_TradeStats_Trade_id)

# find specific dimension, first we have to list all the dimensions
dsd_wits_out = dsd_wits_response.datastructure[dsd_DF_WITS_TradeStats_Trade_id]
dsd_wits_out.dimensions.aslist()
#with pd.option_context('display.max_rows', None ):
 #dsd_wits_response.write().codelist

# then we need to see the codelist for the dimension. 
# In our case, for example, we want to know which indicators are availble.

with pd.option_context('display.max_rows', None):
 dsd_wits_response.write().codelist

# We may see from the above list, there is indeed a variable called BFDA_USD
wits_response = wits_rqst.data('DF_WITS_TradeStats_Trade', key={'REPORTER' : 'USA'})

