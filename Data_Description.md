# README for Trilemma related data

* folder: \Dropbox\InProgress\Data\Trilemma

# Data Description

## file: fx_ir_source_data.mdb
* Source: Global Financial Data (GFD), borrowed account from UCD library
* Includes foreign exchange rate (vs. USD) and 3-month short-term interest rate for countries covered in the database GFD
* The country data available is subject to overlapping of both FX and IR coverage. 
* Current coverage for this file is as follows.

Monthly Data 

Country | Date
---- |  ---
Albania	|	1/7/1994	-	1/1/2015	
Algeria	|	1/6/1998	-	1/1/2015	
Angola	|	1/9/2000	-	1/9/2017	
Argentina	|	1/9/2002	-	1/10/2017	
Armenia	|	1/9/1995	-	1/11/2017	
Australia	|	1/7/1928	-	1/11/2017	
Azerbaijan	|	1/5/1997	-	1/8/2017	
Bahrain	|	1/6/1987	-	1/9/2017	
Bangladesh	|	1/1/1984	-	1/10/2017	
Barbados	|	1/12/1966	-	1/12/2014	
Belgium	|	1/1/1948	-	1/11/2017	
Belize	|	1/12/1978	-	1/1/2015	
Bolivia	|	1/1/1994	-	1/1/2015	
Botswana	|	1/2/1996	-	1/9/2017	
Brazil	|	1/4/1922	-	1/11/2017	
Burundi	|	1/12/2001	-	1/6/2017	
Canada	|	1/3/1934	-	1/11/2017	
China	|	1/1/2002	-	1/11/2017	
Colombia	|	1/1/1998	-	1/11/2017	
Croatia	|	1/12/2000	-	1/10/2017	
Czech Republic	|	1/8/1993	-	1/2/2017	
Egypt	|	1/1/1991	-	1/11/2017	
Fiji	|	1/1/1975	-	1/6/2015	
Finland	|	1/2/2012	-	1/5/2013	
France	|	1/1/1931	-	1/11/2017	
Germany	|	1/1/1953	-	1/11/2017	
Ghana	|	1/1/1978	-	1/4/2014	
Greece	|	1/1/1980	-	1/11/2017	
Guyana	|	1/1/1972	-	1/8/2017	
Haiti	|	1/11/1996	-	1/10/2017	
Hungary	|	1/12/1988	-	1/11/2017	
Iceland	|	1/6/1987	-	1/1/2013	
India	|	1/1/1931	-	1/11/2017	
Iraq	|	1/1/2004	-	1/10/2013	
Ireland	|	1/12/1969	-	1/12/2008	
Israel	|	1/1/1992	-	1/11/2017	
Italy	|	1/1/1940	-	1/11/2017	
Jamaica	|	1/1/1953	-	1/10/2017	
Japan	|	1/1/1960	-	1/11/2017	
Jordan	|	1/12/2000	-	1/6/2015	
Kazakhstan	|	1/4/1994	-	1/9/2017	
Kyrgyzstan	|	1/1/1994	-	1/2/2016	
Latvia	|	1/5/1994	-	1/11/2015	
Lebanon	|	1/12/1977	-	1/9/2017	
Lithuania	|	1/7/1994	-	1/9/2014	
Madagascar	|	1/8/2000	-	1/9/2017	
Malaysia	|	1/1/1961	-	1/11/2017	
Malta	|	1/11/1987	-	1/7/2017	
Mauritius	|	1/12/1996	-	1/8/2016	
Mexico	|	1/1/1978	-	1/11/2017	
Morocco	|	1/1/2008	-	1/11/2016	
Mozambique	|	1/1/2000	-	1/1/2015	
Namibia	|	1/5/1991	-	1/1/2015	
Nepal	|	1/1/1981	-	1/10/2016	
Netherlands	|	1/1/1941	-	1/11/2017	
New Zealand	|	1/3/1978	-	1/11/2017	
Nigeria	|	1/12/1970	-	1/9/2017	
Norway	|	1/1/1984	-	1/11/2017	
Pakistan	|	1/3/1991	-	1/11/2017	
Philippines	|	1/1/1976	-	1/11/2017	
Poland	|	1/5/1991	-	1/11/2017	
Portugal	|	1/8/1985	-	1/8/2014	
Russian Federation	|	1/7/1994	-	1/11/2017	
Rwanda	|	1/1/1999	-	1/10/2017	
Serbia	|	1/12/1997	-	1/9/2017	
Singapore	|	1/12/1987	-	1/11/2017	
Slovenia	|	1/5/1998	-	1/10/2017	
South Africa	|	1/1/1936	-	1/11/2017	
Spain	|	1/7/1982	-	1/11/2017	
Sri Lanka	|	1/4/1981	-	1/10/2017	
Sweden	|	1/1/1955	-	1/11/2017	
Tanzania, United Republic of	|	1/12/1993	-	1/8/2017	
Thailand	|	1/1/1977	-	1/11/2017	
Trinidad and Tobago	|	1/12/1964	-	1/10/2014	
Tunisia	|	1/1/1990	-	1/11/2016	
Turkey	|	1/9/1985	-	1/9/2014	
Uganda	|	1/1/1980	-	1/11/2017	
Ukraine	|	1/4/2011	-	1/8/2015	
United Kingdom	|	1/1/1900	-	1/11/2017	
Zambia	|	1/1/1978	-	1/10/2017	


*last updated 12/12/2017 