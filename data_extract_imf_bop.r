# clear all variables
rm(list = ls(all = TRUE))
# Install IMF data package
libraries = c("imfr","countrycode", "plyr", "zoo")
lapply(libraries, function(x) if (!(x %in% installed.packages())) {
  install.packages(x)
})
lapply(libraries, library, quietly = TRUE, character.only = TRUE)


setwd("D:/Donglai/bitbucket/trilemma")

# ### Getting list from IMF database ###
# IMF.IFS.codelist <- imf_codelist(database_id = 'IFS') # Retrieve codelist from IFS database
# IMF.BOP.codelist <- imf_codelist(database_id = 'BOP') # Retrieve codelist from BOP database
# IMF.IFS.codes <- imf_codes(codelist = IMF.IFS.codelist[4,1])
# IMF.BOP.codes <- imf_codes(codelist = IMF.BOP.codelist[4,1])
# # write.csv(BOP_codes, "BOP_codes.csv")

# Codes to consult the definition of the inidcator
dstr_bop =  DataStructureMethod("BOP")
names(dstr_bop)                    
CodeSearch(dstr_bop, "CL_INDICATOR_BOP" , "BFPLE_BP6_USD")


### Quarterly data (flow)
BFP.LD <- imf_data(database_id = 'BOP', indicator = 'BFPLD_BP6_USD', country = "all", 
                   start = 1970, end = 2017, freq = "Q")
BFP.AD <- imf_data(database_id = 'BOP', indicator = 'BFPAD_BP6_USD', country = "all", 
                   start = 1970, end = 2017, freq = "Q")
BFP.LE <- imf_data(database_id = 'BOP', indicator = 'BFPLE_BP6_USD', country = "all",
                   start = 1970, end = 2017, freq = "Q")
BFP.AE <- imf_data(database_id = 'BOP', indicator = 'BFPAE_BP6_USD', country = "all", 
                   start = 1970, end = 2017, freq = "Q")
BFD.A <- imf_data(database_id = 'BOP', indicator = 'BFDA_BP6_USD', country = "all", 
                  start = 1970, end = 2017, freq = "Q")
BFD.L <- imf_data(database_id = 'BOP', indicator = 'BFDL_BP6_USD', country = "all", 
                  start = 1970, end = 2017, freq = "Q")
BFP.L <- imf_data(database_id = 'BOP', indicator = 'BFPL_BP6_USD', country = "all", 
                  start = 1970, end = 2017, freq = "Q")
BFP.A <- imf_data(database_id = 'BOP', indicator = 'BFPA_BP6_USD', country = "all", 
                  start = 1970, end = 2017, freq = "Q")
BFR.A <- imf_data(database_id = 'BOP', indicator = 'BFRA_BP6_USD', country = "all", 
                  start = 1970, end = 2017, freq = "Q")

### Merge data into one file and count number of observations
BF.L <- Reduce(function(x,y) merge(x,y,by=(c("iso2c","year_quarter")),all=FALSE), list(BFP.LD, BFP.LE,BFD.L,BFP.L))
row.with.na <- apply(BF.L, 1, function(x){any(is.na(x))})
sum(row.with.na)
BF.L <- BF.L[!row.with.na,]
BF.L$year_quarter <- as.yearqtr(BF.L$year_quarter, format = "%Y-Q%q")
BF.L$iso2c <- countrycode(BF.L$iso2c, 'iso2c' , 'iso3c')
BF.L.obs <- rename(as.data.frame(table(BF.L$iso2c)),c("Var1"="iso3c","Freq"="BF.L.obs"))
BF.L.obs$iso3c <- countrycode(BF.L.obs$iso3c, 'iso2c' , 'iso3c')
BF.L.obs.20 <- BF.L.obs[which(BF.L.obs$BF.L.obs>=20),]


BF.A <- Reduce(function(x,y) merge(x,y,by=(c("iso2c","year_quarter")),all=FALSE), list(BFP.AD,BFP.AE,BFD.A,BFP.A))
row.with.na <- apply(BF.A, 1, function(x){any(is.na(x))})
sum(row.with.na)
BF.A <- BF.A[!row.with.na,]
BF.A$year_quarter <- as.yearqtr(BF.A$year_quarter, format = "%Y-Q%q")
BF.A$iso2c <- countrycode(BF.A$iso2c, 'iso2c' , 'iso3c')
BF.A.obs <- rename(as.data.frame(table(BF.A$iso2c)),c("Var1"="iso3c","Freq"="BF.A.obs"))
BF.A.obs$iso3c <- countrycode(BF.A.obs$iso3c, 'iso2c' , 'iso3c')
# write to disk
write.csv(BF.A, "BFA.csv")
write.csv(BF.L, "BFL.csv")
