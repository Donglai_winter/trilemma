# import required pkg #
import pandas as pd
import pandasdmx as psdmx

from pandasdmx import Request as rqst 
import urllib.request, json, pprint

# show available database from pandaSDMX
rqst.list_agencies()

# Dive into IMF data
imf = 'IMF_SDMXCENTRAL'
imf_rqst = rqst('IMF_SDMXCENTRAL')
imf_categoryscheme = imf_rqst.categoryscheme()  

# Show dataflow from IMF
imf_dataflow_list = imf_categoryscheme.write().dataflow
# A glance at the dataflow def_list
imf_categoryscheme.write().dataflow
# a glance at the category scheme
imf_categoryscheme.write().categoryscheme

# Show particular dataflow def. (DFD) for dataflow of interest
# we are interested in the capital in/out-flow so should be in the financial sector
list(imf_categoryscheme.categoryscheme.CS_SDMX_CENTRAL['1']) 
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
 print(imf_dataflow_list) #read dataflow_id from here

# Getting the data structure definition (DSD)
# Take the BOP_GBPM6, balance of payment 6, for example
dsd_BOP_GBPM6_id = imf_categoryscheme.dataflow.BOP_GBPM6.structure.id # it is actually 'ECOFIN_DSD'
refs = dict(reference = 'all')
dsd_BOP_GBPM6_response = imf_rqst.datastructure(resource_id = dsd_BOP_GBPM6_id, params = refs)

# find specific dimension, first we have to list all the dimensions
dsd_BOP_GBPM6_out = dsd_BOP_GBPM6_response.datastructure[dsd_BOP_GBPM6_id]
dsd_BOP_GBPM6_out.dimensions.aslist()
with pd.option_context('display.max_rows', None ):
 dsd_BOP_GBPM6_response.write().codelist

# then we need to see the codelist for the dimension. 
# In our case, for example, we want to know BFDA_USD: Balance of Payments, Financial Account, Direct Investment, Abroad, Net, US Dollars
# Consult https://sdmxcentral.imf.org/data/datastructure.html for all codes
with pd.option_context('display.max_rows', None):
 dsd_BOP_GBPM6_response.write().codelist.loc['REF_AREA']

# We may see from the above list, there is indeed a variable called BFDA_USD
bfda_response = imf_rqst.data('GBPM6', key={'REF_AREA' : 'ZA'})

# The above command is not working, what is tested working is as the following:
test = imf_rqst.data('IIP_GBPM6')

# Test for JSON source which is thought to be more complete

with urllib.request.urlopen("http://dataservices.imf.org/REST/SDMX_JSON.svc/Dataflow") as url:
    imf_ids = json.loads(url.read().decode())
    #pprint(imf_ids)
dfd_imf_list = imf_ids['Structure']['Dataflows']['Dataflow']
print(dfd_imf_list)
# to sort it in a dataframe
df_imf_dataflow = pd.DataFrame.from_dict(dfd_imf_list, orient = 'columns')

# Obtain the codelist
def imf_codelist(database_id, return_raw = False):    
    if isna(database_id): 
        return('Must supply database_id.\n\nUse imf_ids to find.')
    else :
       url = 'http://dataservices.imf.org/REST/SDMX_JSON.svc/DataStructure/%s'+ database_id
           with urllib.request.urlopen(url) as urldown:
               raw_dl = json.loads(urldown.read().decode())
        if not return_raw: 
        codelist = raw_dl['Structure']$CodeLists$CodeList$`@id`
        codelist_description = raw_dl$Structure$CodeLists$CodeList$Name$`#text`

        codelist_df = pd.DataFrame({'codelist' : codelist,
                                  'description' = codelist_description})
            return(codelist_df)
                else :
                   return(raw_dl)
          
